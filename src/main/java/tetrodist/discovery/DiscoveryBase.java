package tetrodist.discovery;

import java.io.IOException;
import java.net.Inet4Address;
import java.net.Inet6Address;
import java.net.InetAddress;
import java.net.InterfaceAddress;
import java.net.NetworkInterface;
import java.net.ProtocolFamily;
import java.net.SocketException;
import java.net.StandardSocketOptions;
import java.net.UnknownHostException;
import java.nio.ByteBuffer;
import java.nio.channels.DatagramChannel;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.logging.Logger;

import org.msgpack.MessagePack;

public abstract class DiscoveryBase {
	public static final String MULTICAST_IPADDR6 = "FF02:0:0:0:0:0:0:1";

	protected int port;
	protected String name;
	
	protected Logger logger = Logger.getGlobal();
	protected byte[] multicastAddr6;
	
	private MessagePack msgpack = new MessagePack();
	
	public DiscoveryBase() throws IOException {
		InetAddress addr = InetAddress.getByName(DiscoveryBase.MULTICAST_IPADDR6);
		this.multicastAddr6 = addr.getAddress();
	}
	
	public int getPort() {
		return port;
	}
	
	public void setPort(int port) {
		this.port = port;
	}

	public String getName() {
		return this.name;
	}
	
	public void setName(String name) {
		this.name = name;
	}

	public Logger getLogger() {
		return this.logger;
	}

	public void setLogger(Logger l) {
		this.logger = l;
	}
	
	public void throwing(Throwable t) {
		StackTraceElement ste = t.getStackTrace()[0];
		this.logger.throwing(ste.getClassName(), ste.getMethodName(), t);
	}

	protected DatagramChannel openChennel(ProtocolFamily family) throws IOException {
		DatagramChannel dch = DatagramChannel.open(family);
		dch.configureBlocking(false);
		dch.setOption(StandardSocketOptions.SO_BROADCAST, true);
		dch.setOption(StandardSocketOptions.SO_REUSEADDR, true);
		return dch;
	}
	
	protected ByteBuffer createMessageSolicit() throws IOException {
		DiscoveryMessage msg = new DiscoveryMessage();
		msg.solicit = true;
		msg.advertise = false;
		msg.port = this.port;
		msg.name = this.name;

		return ByteBuffer.wrap(msgpack.write(msg));
	}
	
	protected ByteBuffer createMessageAdvertise() throws IOException {
		DiscoveryMessage msg = new DiscoveryMessage();
		msg.solicit = false;
		msg.advertise = true;
		msg.port = this.port;
		msg.name = this.name;

		return ByteBuffer.wrap(msgpack.write(msg));
	}
	
	protected DiscoveryMessage decodeMessage(ByteBuffer dst) throws IOException {
		dst.rewind();
		return msgpack.read(dst, DiscoveryMessage.class);
	}
	
	protected Iterable<InetAddress> getMulticast(Inet6Address iaddr) {
		try {
			NetworkInterface nif = NetworkInterface.getByInetAddress(iaddr);
			InetAddress mcast6 = Inet6Address.getByAddress(null, this.multicastAddr6, nif);
			return Arrays.asList(mcast6);
		} catch (SocketException|UnknownHostException e) {
			this.throwing(e);
			return Collections.emptyList();
		}
	}
	
	protected Iterable<InetAddress> getMulticast(Inet4Address iaddr) {
		NetworkInterface nif;
		try {
			nif = NetworkInterface.getByInetAddress(iaddr);
		} catch (SocketException e) {
			this.throwing(e);
			return Collections.emptyList();
		}

		ArrayList<InetAddress> mcasts = new ArrayList<>();
		for (InterfaceAddress nicaddr : nif.getInterfaceAddresses()) {
			InetAddress bcast = nicaddr.getBroadcast();
			if (bcast != null) {
				mcasts.add(bcast);
			}
		}

		return mcasts;
	}

	protected Iterable<NetworkInterface> getMulticastInterfaces() {
		Enumeration<NetworkInterface> nifs;
		try {
			nifs = NetworkInterface.getNetworkInterfaces();
		} catch (SocketException e) {
			this.throwing(e);
			return Collections.emptyList();
		}

		ArrayList<NetworkInterface> list = Collections.list(nifs);
		Iterator<NetworkInterface> iter = list.iterator();
		while (iter.hasNext()) {
			NetworkInterface nif = iter.next();
			try {
				if (nif.isLoopback()) {
					iter.remove();
				}
			} catch (SocketException e) {
				iter.remove();
			}
		}

		return list;
	}

	protected Iterable<InetAddress> getSolicitAddresses6() {
		ArrayList<InetAddress> addresses = new ArrayList<InetAddress>();

		for (NetworkInterface nif : this.getMulticastInterfaces()) {
			try {
				InetAddress address = Inet6Address.getByAddress(null, this.multicastAddr6, nif);
				addresses.add(address);
			} catch (UnknownHostException e) {
			}
		}
		
		return addresses;
	}

	protected Iterable<InetAddress> getSolicitAddresses4() {
		ArrayList<InetAddress> addresses = new ArrayList<InetAddress>();

		for (NetworkInterface nif : this.getMulticastInterfaces()) {
			for (InterfaceAddress nicaddr : nif.getInterfaceAddresses()) {
				InetAddress address = nicaddr.getBroadcast();
				if (address != null) {
					addresses.add(address);
				}
			}
		}
		
		return addresses;
	}
}
