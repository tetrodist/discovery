package tetrodist.discovery;

import java.io.Closeable;
import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.SocketAddress;
import java.net.StandardProtocolFamily;
import java.nio.ByteBuffer;
import java.nio.channels.DatagramChannel;
import java.nio.channels.SelectableChannel;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.util.Iterator;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.logging.Level;

import org.msgpack.MessageTypeException;

public class Advertise extends DiscoveryBase implements Closeable {
	public static final int PORT = 6834;
	public static final int DISCOVERY_SIZE = 512;
	public static final long MINIMUM_INTERVAL = 1000;

	private Selector selector;
	private DatagramChannel channel6;
	private DatagramChannel channel4;

	private ExecutorService executor;

	public Advertise() throws IOException {
		this.selector = Selector.open();

		this.channel6 = this.openChennel(StandardProtocolFamily.INET6);
		this.channel6.bind(new InetSocketAddress(Advertise.PORT));
		this.channel6.register(this.selector, SelectionKey.OP_READ);

		this.channel4 = this.openChennel(StandardProtocolFamily.INET);
		this.channel4.bind(new InetSocketAddress(Advertise.PORT));
		this.channel4.register(this.selector, SelectionKey.OP_READ);

		this.executor = Executors.newSingleThreadExecutor();
	}
	
	public void open()
	{
		final Selector sel = this.selector;

		final DatagramChannel ch6 = this.channel6;
		final Iterable<InetAddress> addr6 = Advertise.this.getSolicitAddresses6();

		final DatagramChannel ch4 = this.channel4;
		final Iterable<InetAddress> addr4 = Advertise.this.getSolicitAddresses4();

		this.executor.submit(new Runnable() {
			private boolean advertise = false;

			@Override
			public void run() {
				while (!Thread.interrupted()) {
					try {
						sel.select();
					} catch (IOException e) {
						return;
					}
					
					if (Thread.interrupted()) {
						return;
					}
					
					Set<SelectionKey> keys = sel.selectedKeys();
					Iterator<SelectionKey> iter = keys.iterator();
					while (iter.hasNext()) {
						SelectionKey key = iter.next();
						iter.remove();
						
						if (key.isReadable()) {
							this.handleReadable(key.channel());
						}
					}
					
					if (this.advertise) {
						this.advertise = false;

						Advertise.this.advertise(ch6, addr6);
						Advertise.this.advertise(ch4, addr4);

						try {
							Thread.sleep(Advertise.MINIMUM_INTERVAL);
						} catch (InterruptedException e) {
							return;
						}
					}
				}
			}
			
			private void handleReadable(SelectableChannel sch) {
				DatagramChannel ch;
				try {
					ch = (DatagramChannel)sch;
				} catch (ClassCastException e) {
					Advertise.this.throwing(e);
					return;
				}

				ByteBuffer dst = ByteBuffer.allocate(Advertise.DISCOVERY_SIZE);
				while (true) {
					try {
						SocketAddress remote = ch.receive(dst);

						if (remote == null) {
							return;
						}
					} catch (IOException e) {
						Advertise.this.throwing(e);
						return;
					}
					
					try {
						DiscoveryMessage discmsg = Advertise.this.decodeMessage(dst);
	
						if (discmsg.solicit == true) {
							this.advertise = true;
						}
					} catch (IOException|MessageTypeException e) {
						Advertise.this.throwing(e);
					}
				}
			}
		});
	}

	private void advertise(DatagramChannel ch, Iterable<InetAddress> addresses) {
		ByteBuffer src;
		try {
			src = this.createMessageAdvertise();
		} catch (IOException e) {
			this.throwing(e);
			return;
		}

		for (InetAddress address : addresses) {
			InetSocketAddress remote = new InetSocketAddress(address, Solicit.PORT);
			try {
				ch.send(src, remote);
				if (this.logger.isLoggable(Level.INFO)) {
					this.logger.info("Advertising " + remote + "...");
				}
			} catch (IOException e) {
				this.throwing(e);
			}
		}
	}
	
	@Override
	public void close() throws IOException {
		this.executor.shutdownNow();
	}
}
