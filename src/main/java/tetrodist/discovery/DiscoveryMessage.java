package tetrodist.discovery;

import org.msgpack.annotation.Message;

@Message
public class DiscoveryMessage {
	public boolean solicit = false;
	public boolean advertise = false;

	public int port = 0;
	public String name = "";
}
