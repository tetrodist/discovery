package tetrodist.discovery;
import java.io.BufferedReader;
import java.io.Closeable;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;


public class CommunicationReceiver implements Closeable {
	private final ServerSocket serverSocket;

	private final ExecutorService executor;
	
	public CommunicationReceiver(ServerSocket ss) {
		this.serverSocket = ss;

		this.executor = Executors.newCachedThreadPool();
	}
	
	public void open() {
		this.openServer(this.serverSocket);
	}
	
	private void openServer(final ServerSocket ss) {
		final ExecutorService exe = this.executor;
		final List<Socket> conns = new ArrayList<Socket>();
		exe.submit(new Runnable() {
			@Override
			public void run() {
				try {
					while (!Thread.interrupted()) {
						Socket s;
						try {
							s = ss.accept();
						} catch (IOException e) {
							return;
						}

						try {
							CommunicationReceiver.this.openConnection(s);
							conns.add(s);
						} catch (IOException e) {
							this.closeForce(s);
							System.out.println("Connection closed.");
						}
					}
				} finally {
					for (Socket s : conns) {
						this.closeForce(s);
					}
					System.out.println("Server closed.");
				}
			}

			private void closeForce(Closeable c) {
				try {
					c.close();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	
	private void openConnection(final Socket s) throws IOException {
		final InputStream in = s.getInputStream();
		final InputStreamReader inr = new InputStreamReader(in);
		final BufferedReader br = new BufferedReader(inr);
		System.out.println(br.readLine());
		this.executor.submit(new Runnable() {
			@Override
			public void run() {
				try {
					while (!Thread.interrupted()) {
						String str;
						try {
							str = br.readLine();
						} catch (SocketException e) {
							System.out.println("Socket closed.");
							return;
						} catch (IOException e) {
							e.printStackTrace();
							return;
						}
						
						if (str == null) {
							System.out.println("Socket closed by remote.");
							return;
						}
	
						System.out.println(str);
					}
				} finally {
					this.closeForce(br);
					this.closeForce(inr);
					this.closeForce(in);
					System.out.println("Connection closed.");
				}
			}

			private void closeForce(Closeable c) {
				try {
					c.close();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	@Override
	public void close() {
		this.executor.shutdownNow();

		try {
			this.serverSocket.close();
		} catch (IOException e) {
		}
	}
}
