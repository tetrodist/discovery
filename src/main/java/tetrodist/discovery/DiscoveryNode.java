package tetrodist.discovery;

import java.net.InetAddress;

public class DiscoveryNode {
	private final InetAddress address;
	private final int port;
	private final String name;
	
	public DiscoveryNode(InetAddress addr, int port, String name) {
		this.address = addr;
		this.port = port;
		this.name = name;
	}
	
	public InetAddress getAddress() {
		return address;
	}
	
	public int getPort() {
		return port;
	}
	
	public String getName() {
		return name;
	}
	
	@Override
	public String toString() {
		StringBuilder b = new StringBuilder();
		b.append(this.name);
		b.append("@");
		b.append(this.address);
		b.append(":");
		b.append(this.port);
		return b.toString();
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj instanceof DiscoveryNode) {
			DiscoveryNode o = (DiscoveryNode)obj;
			return this.address.equals(o.address) && this.port == o.port && this.name.equals(o.name);
		}
		return false;
	}
	
	@Override
	public int hashCode() {
		return this.address.hashCode() + this.port + this.name.hashCode();
	}
}
