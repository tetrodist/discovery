package tetrodist.discovery;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.Closeable;
import java.io.IOException;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Random;
import java.util.Set;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.Timer;


public class Main {
	public static void main(String[] args) {
		final Main m = new Main();
		m.start();
	}
	
	private List<Closeable> resources;
	private Solicit solicit;
	
	public Main() {
		Logger logger = Logger.getGlobal();
		logger.setLevel(Level.WARNING);
		this.resources = new ArrayList<Closeable>();
	}

	public void start() {
		JPanel p = new JPanel();
		
		final JTextField textFieldAddr = this.createTextFieldAddr();
		p.add(textFieldAddr);

		final JTextField textFieldServerName = this.createTextFieldServerName();
		p.add(textFieldServerName);

		JButton buttonAckReceiver = this.createButtonAdvertise(textFieldServerName, resources);
		p.add(buttonAckReceiver);
		
		JButton buttonAckSender = this.createButtonSolicit(textFieldAddr, this.resources);
		p.add(buttonAckSender);
		
		JButton buttonCommnicationReceiver = this.createButtonCommunicationReceiver(resources);
		p.add(buttonCommnicationReceiver);
		
		JButton buttonCommnicationSender = this.createButtonCommunicationSender(textFieldAddr, resources);
		p.add(buttonCommnicationSender);

		JButton buttonExit = this.createButtonExit();
		p.add(buttonExit);
		
		final DefaultListModel<DiscoveryNode> model = new DefaultListModel<>();
		JList<DiscoveryNode> listServers = new JList<>();
		listServers.setModel(model);
		p.add(listServers);
		
		Timer time = new Timer(1000, new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (Main.this.solicit == null) return;
				Set<DiscoveryNode> nodes = Main.this.solicit.getNodes();
				Set<DiscoveryNode> view = new HashSet<>(Collections.list(model.elements()));
				
				Set<DiscoveryNode> removing = new HashSet<>(view);
				removing.removeAll(nodes);

				Set<DiscoveryNode> adding = new HashSet<>(nodes);
				adding.removeAll(view);

				for (DiscoveryNode remote : removing) {
					model.removeElement(remote);
				}

				for (DiscoveryNode remote : adding) {
					model.addElement(remote);
				}
			}
		});
		time.start();

		final JFrame window = new JFrame();
		window.setContentPane(p);
		window.setSize(300, 300);
		window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		window.setVisible(true);
	}

	private JTextField createTextFieldAddr() {
		JTextField textField = new JTextField();
		textField.setText("localhost");
		return textField;
	}

	private JTextField createTextFieldServerName() {
		String hostname;
		try {
			hostname = InetAddress.getLocalHost().getHostName();
		} catch (UnknownHostException e) {
			hostname = "localhost";
		}

		JTextField textField = new JTextField();
		textField.setText(hostname);
		return textField;
	}
	
	private JButton createButtonSolicit(final JTextField txtfldAddr, final List<Closeable> resources) {
		JButton button = new JButton();
		button.setText("Solicit");
		button.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				try {
					solicit = new Solicit();
					System.out.println("Solicit started...");
					solicit.open();
					resources.add(solicit);
				} catch (IOException ex) {
					ex.printStackTrace();
				}
			}
		});
		
		return button;
	}
	
	private JButton createButtonAdvertise(final JTextField txtfldName, final List<Closeable> resources) {
		JButton button = new JButton();
		button.setText("Advertise");
		button.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				try {
					Advertise adv = new Advertise();
					System.out.println("Advertise started...");
					adv.setPort(8080);
					adv.setName(txtfldName.getText());
					adv.open();
					resources.add(adv);
				} catch (IOException ex) {
					ex.printStackTrace();
				}
			}
		});
		
		return button;
	}
	
	private JButton createButtonCommunicationReceiver(final List<Closeable> resources) {
		JButton button = new JButton();
		button.setText("CommunicationReceiver");
		button.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				try {
					ServerSocket ds = new ServerSocket(8080);
					CommunicationReceiver commres = new CommunicationReceiver(ds);
					System.out.println("CommunicationReceiver started...");
					commres.open();
					resources.add(commres);
				} catch (IOException ex) {
					ex.printStackTrace();
				}
			}
		});
		
		return button;
	}
	
	private JButton createButtonCommunicationSender(final JTextField txtfldAddr, final List<Closeable> resources) {
		JButton button = new JButton();
		button.setText("Send");
		final ScheduledExecutorService ses = Executors.newSingleThreadScheduledExecutor();
		button.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				try {
					String host = txtfldAddr.getText();
					InetAddress addr = InetAddress.getByName(host);
					Socket s = new Socket(addr, 8080);
					final CommunicationSender commsend = new CommunicationSender(s);
					resources.add(commsend);
					final int id = new Random().nextInt();
					ses.scheduleAtFixedRate(new Runnable() {
						@Override
						public void run() {
							commsend.send(Integer.toString(id));
						}
					}, 0, 10, TimeUnit.MILLISECONDS);
				} catch (IOException ex) {
					ex.printStackTrace();
				}
			}
		});
		
		return button;
	}
	
	private JButton createButtonExit() {
		JButton button = new JButton();
		button.setText("Exit");
		final List<Closeable> cl = this.resources;
		button.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				for (Closeable c : cl) {
					this.closeForce(c);
				}
			}

			private void closeForce(Closeable c) {
				try {
					c.close();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
		
		return button;
	}
}
